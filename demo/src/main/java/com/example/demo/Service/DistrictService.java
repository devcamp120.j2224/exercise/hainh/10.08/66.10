package com.example.demo.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.District;
import com.example.demo.Model.Province;
import com.example.demo.Respository.iDistrictRespository;
import com.example.demo.Respository.iProvinceRespository;

@Service
public class DistrictService {
      
    @Autowired
    iProvinceRespository iProvinceRespository;

    @Autowired
    iDistrictRespository iDistrictRespository;

    public District createDistrict(int id , District district) {
        Optional<Province> provinceData = iProvinceRespository.findById(id);
        Optional<District> districtFound = iDistrictRespository.findById(district.getId());

        if (provinceData.isPresent()) {
            if (districtFound.isPresent()) {
                return null;
            }
            Province provinceNew = provinceData.get();
            district.setProvince(provinceNew);
            District newDistrict = iDistrictRespository.save(district);

            return newDistrict;
        }
            
        else {
            return null;
        }
                   
    }        
   
    public District updateDistrict(int id , District district) {

        Optional<District> districtData = iDistrictRespository.findById(id);

        if (districtData.isPresent()) {
			District newDistrict = districtData.get();
			newDistrict.setName(district.getName());
			newDistrict.setPrefix(district.getPrefix());
            return iDistrictRespository.save(newDistrict);
            
		} else {
			return null ;
		}
    }

   
    public District getDistrictById(int id) {
        Optional<District> dist = iDistrictRespository.findById(id);
        if (dist.isPresent()) {
            District district = dist.get();
            return district;
        } else {
            return null;
        }
    }


}
