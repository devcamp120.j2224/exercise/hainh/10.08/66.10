package com.example.demo.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.District;
import com.example.demo.Model.Ward;
import com.example.demo.Respository.iDistrictRespository;
import com.example.demo.Respository.iWardRespository;

@Service
public class WardService {
    
    @Autowired
    iWardRespository iWardRespository;

    @Autowired
    iDistrictRespository iDistrictRespository;

    public Ward getWardById(int id) {
        Optional<Ward> wards = iWardRespository.findById(id);
        if (wards.isPresent()) {
            Ward ward = wards.get();
            return ward;
        } else {
            return null;
        }
    }


    public Ward createWard(int id , Ward wards) {
        Optional<District> districtData = iDistrictRespository.findById(id);

        if (districtData.isPresent()) {
            Ward newWard = new Ward();
            newWard.setName(wards.getName());
            newWard.setPrefix(wards.getPrefix());
            newWard.setDistrict(wards.getDistrict());
            
            District district = districtData.get();
            newWard.setDistrict(district);
            newWard.setDistrictName(district.getName());
            
            Ward savedRole = iWardRespository.save(newWard);
            return savedRole ;
        }
            
        else {
            return null;
        }
                   
    }

    public Ward updateWard(int id , Ward wards) {

        Optional<Ward> wardData = iWardRespository.findById(id);

        if (wardData.isPresent()) {
			Ward newWard = wardData.get();
			newWard.setName(wards.getName());
			newWard.setPrefix(wards.getPrefix());
            return iWardRespository.save(newWard);
            
		} else {
			return null ;
		}
    }
  
}
