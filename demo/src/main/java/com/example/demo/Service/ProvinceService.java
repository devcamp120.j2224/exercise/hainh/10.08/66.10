package com.example.demo.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import com.example.demo.Model.Province;
import com.example.demo.Respository.iProvinceRespository;

@Service
public class ProvinceService {
    
    @Autowired 
    iProvinceRespository iProvinceRespository ;

    public List<Province> getAllProvince(){
        List<Province> provinceList = new ArrayList<>();
        iProvinceRespository.findAll().forEach(provinceList::add);
        return provinceList;
    }

    public Province getProvinceById(int id) {
        Optional<Province> provinceFind = iProvinceRespository.findById(id);
        if (provinceFind.isPresent()) {
            Province province = provinceFind.get();
            return province;
        } else {
            return null;
        }
    }

    public Province createProvince(Province province) {
        Optional<Province> provinceData = iProvinceRespository.findById(province.getId());
            if (provinceData.isPresent()){
                return null ;
            }
            else {
                Province newProvince = new Province(province.getCode() , province.getName());
                iProvinceRespository.save(newProvince);
                return newProvince;
            }
        }

    public Province updateProvince(int id , Province province ) {
        Optional<Province> provinceData = iProvinceRespository.findById(id);

        if (provinceData.isPresent()){
            Province provinceUpdate = provinceData.get();
            provinceUpdate.setCode(province.getCode());
            provinceUpdate.setName(province.getName());

            return iProvinceRespository.save(provinceUpdate);
                
        } else {
            return null;
        }

    }    

}
