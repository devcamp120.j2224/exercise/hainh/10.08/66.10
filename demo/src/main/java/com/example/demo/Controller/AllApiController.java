package com.example.demo.Controller;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.District;
import com.example.demo.Model.Province;
import com.example.demo.Model.Ward;
import com.example.demo.Respository.iDistrictRespository;
import com.example.demo.Respository.iProvinceRespository;
import com.example.demo.Respository.iWardRespository;
import com.example.demo.Service.DistrictService;
import com.example.demo.Service.ProvinceService;
import com.example.demo.Service.WardService;

@RestController
@CrossOrigin
public class AllApiController {
    
    @Autowired
    iProvinceRespository iProvinceRes;

    @Autowired
    iDistrictRespository iDistrictRes;

    @Autowired
    iWardRespository iWardRes;


    @Autowired
    ProvinceService provinceService ;

    @Autowired
    DistrictService districtService ;

    @Autowired
    WardService wardService ;
    
    // tìm 1 province bằng distristId
    @GetMapping("province-info")
    public ResponseEntity<Set<District>> getDisStrictList(@RequestParam(name = "districtId") int districtId) {
        try {
            Province province =  iProvinceRes.getProvinceById(districtId);
            if(province!=null){
                return new ResponseEntity<>(province.getDistrict(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    // tìm province bằng id 
    @GetMapping("/provinces/{id}")
    public ResponseEntity<Province> getProvinceById(@PathVariable("id") int id) {
        Province province = iProvinceRes.getProvinceById(id);
        if (province != null) {
			try {
				return new ResponseEntity<Province>(province,HttpStatus.OK);            
			} catch (Exception e) {
				return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);   
			}
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    // lấy danh sách province
    @GetMapping("province")
    public ResponseEntity<List<Province>> getProvinceList(){
        try {
            List<Province> provinceList = provinceService.getAllProvince();
            return new ResponseEntity<>(provinceList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // create province
    @PostMapping("province-create")
    public ResponseEntity<Object> createProvince(@RequestBody Province province) {
       
        try {
            Province provinceCreate = provinceService.createProvince(province);
            
            return new ResponseEntity<Object>(provinceCreate, HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                .body("Can not execute operation about this Entity " + e.getCause().getMessage());
        }
    }

    //update province
    @PutMapping("province-update/{id}")
    public ResponseEntity<Object> updateProvince(@PathVariable (name ="id") int id, @RequestBody Province province) {
        Province provinceUpdate = provinceService.updateProvince(id, province);

        if (provinceUpdate != null){
            try {
                return ResponseEntity.ok(iProvinceRes.save(provinceUpdate));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Can not execute operation of this Entity"+ e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object> (null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }      

    // delete province
    @DeleteMapping("province-delete/{id}")
    public ResponseEntity<Object> deleteProvice(@PathVariable("id") int id){
        Optional<Province> provinceDelete = iProvinceRes.findById(id) ;
        if (provinceDelete.isPresent()) {
            try {
                iProvinceRes.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT) ;
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>("User not found! ", HttpStatus.NOT_FOUND);
        }
    } 

    /* --------------- WARD ----------------*/
    // tìm ward bằng id của district


    @GetMapping("district-info")
    public ResponseEntity<Set <Ward>> getWardList(@RequestParam(name = "id") int wardId) {

        //return new ResponseEntity<>(iDistrictRes.findById(wardId) , HttpStatus.OK);
        try {
            District district =  iDistrictRes.getDistrictById(wardId);
            if(district != null) {
                return new ResponseEntity<>(district.getWards(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
     }

     @GetMapping("/wards/{id}")
     public ResponseEntity<Ward> getWardById(@PathVariable("id") int id) {
         Ward ward = wardService.getWardById(id);
         if (ward != null) {
             try {
                 return new ResponseEntity<Ward>(ward,HttpStatus.OK);            
             } catch (Exception e) {
                 return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);   
             }
         } else {
             return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
 
 

    // create ward by District id

        @PostMapping("ward-create/{id}")
        public ResponseEntity<Object> createWardWithIDistrictId(@PathVariable("id") int id, @RequestBody Ward cWard) {
            try {
                Ward createWard = wardService.createWard(id ,cWard);

                return new ResponseEntity<>(createWard, HttpStatus.CREATED);
                } 
            catch (Exception e) {
                System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
                return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
            }
        }
    //   update ward by wardId 
        @PutMapping("ward-update/{id}")
        public ResponseEntity<Ward> updateWardByWardId(@PathVariable(name = "id") int id ,@RequestBody Ward ward) {

            try {
                return new ResponseEntity<>(wardService.updateWard(id, ward), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
       }

        @DeleteMapping("ward-delete/{id}")
        public ResponseEntity<Object> deleteWard(@PathVariable("id") int id){
           Optional<Ward> wardDelete = iWardRes.findById(id) ;
           if (wardDelete.isPresent()) {
               try {
                iWardRes.deleteById(id);
                   return new ResponseEntity<Object>(HttpStatus.NO_CONTENT) ;
               } catch (Exception e) {
                   return ResponseEntity.unprocessableEntity()
                           .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
               }
           } else {
               return new ResponseEntity<Object>("User not found! ", HttpStatus.NOT_FOUND);
           }
       } 
/* --------------- District ----------------*/

 // create district by Province id


        @GetMapping("/districts/{id}")
        public ResponseEntity<District> getDistrictById(@PathVariable("id") int id) {
            District district = districtService.getDistrictById(id);
            if (district != null) {
                try {
                    return new ResponseEntity<District>(district,HttpStatus.OK);            
                } catch (Exception e) {
                    return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);   
                }
            } else {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        @PostMapping("district-create/{id}")
        public ResponseEntity<Object> createDistrictWithProvinceId(@PathVariable("id") int id, @RequestBody District district) {
            try {
                District districtWard = districtService.createDistrict(id ,district);

                return new ResponseEntity<>(districtWard, HttpStatus.CREATED);
                } 
            catch (Exception e) {
                System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
                return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
            }
        }


        //   update district by districtId 
        @PutMapping("district-update/{id}")
        public ResponseEntity<District> updateDistrictByDistrictId(@PathVariable(name = "id") int id ,@RequestBody District district) {

            try {
                return new ResponseEntity<>(districtService.updateDistrict(id, district), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
        // delete district 
        @DeleteMapping("district-delete/{id}")
        public ResponseEntity<Object> deleteDistrict(@PathVariable("id") int id){
            Optional<District> districtDelete = iDistrictRes.findById(id) ;
            if (districtDelete.isPresent()) {
                try {
                    iDistrictRes.deleteById(id);
                    return new ResponseEntity<Object>(HttpStatus.NO_CONTENT) ;
                } catch (Exception e) {
                    return ResponseEntity.unprocessableEntity()
                            .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                }
            } else {
                return new ResponseEntity<Object>("User not found! ", HttpStatus.NOT_FOUND);
            }
        } 
    
    }
