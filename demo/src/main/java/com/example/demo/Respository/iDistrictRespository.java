package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.District;

public interface iDistrictRespository extends JpaRepository <District , Integer> {
    District getDistrictById(int id);
}
