package com.example.demo.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name= "ward")
public class Ward {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id ;

    @Column(name= "name")
    private String name ;

    @Column(name= "prefix")
    private String prefix ;

    @ManyToOne
    @JoinColumn(name="district_id")
    @JsonBackReference
    private District district ;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    // chặn đệ quy
    @JsonIgnore
    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Ward() {
    }

    public Ward(int id, String name, String prefix, District district) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.district = district;
    } 

    // tạo ra 1 biến districtName
    @Transient
    private String districtName;

    // làm set get của biến mới tạo ra
    // hàm get thì lấy get của district trả về chấm với thuộc tính getName của class District
    @JsonIgnore
    public String getDistrictName() {
        return getDistrict().getName();
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

}

